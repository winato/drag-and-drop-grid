import '../styles/index.scss';

function DragNSort (config) {
  this.$activeItem = null;
  this.$container = config.container;
  this.$items = this.$container.querySelectorAll('.' + config.itemClass);
  this.dragStartClass = config.dragStartClass;
  this.dragEnterClass = config.dragEnterClass;
}

function getAttr (element, dataAttr) {
  return element.getAttribute(dataAttr) || element.parentNode.getAttribute(dataAttr);
};

function getElement (x, y) {
  return document.elementFromPoint(x, y);
};

function getTranslateValue(translateString, dir) {

  const a = dir === 'x' ? 2 : 1;

  const n = translateString.indexOf("(");
  const n1 = translateString.indexOf(",");

  return parseInt(translateString.slice(n + 1, n1 - a)) || 0;
};

let fromX = 0;
let fromY = 0;
let toX = 0;
let toY = 0;

let finalChildAStyle = [];

let finalChildBStyle = [];

const emptyCoordinates = {
  x: null,
  y: null,
};

function createArray() {
  const allCards = document.querySelectorAll('.drag-item');

  Array.from(allCards).forEach((i) => {
    finalChildBStyle.push(JSON.parse(JSON.stringify(emptyCoordinates)));
    finalChildAStyle.push(JSON.parse(JSON.stringify(emptyCoordinates)));
  });
};

Object.defineProperty(Element.prototype, 'documentOffsetTop', {
  get: function () {
    const transformValue = getTranslateValue(this.style.transform, 'y'); // May be useful in future... may be...

    return this.offsetTop + (this.offsetParent ? this.offsetParent.documentOffsetTop : 0);
  }
});

Object.defineProperty(Element.prototype, 'documentOffsetLeft', {
  get: function () {
    const transformValue = getTranslateValue(this.style.transform, 'x');

    return this.offsetLeft + (this.offsetParent ? this.offsetParent.documentOffsetLeft : 0) + transformValue;
  }
});

DragNSort.prototype.removeClasses = function () {
    [].forEach.call(this.$items, function ($item) {
        $item.classList.remove(this.dragStartClass, this.dragEnterClass);
  }.bind(this));
};

DragNSort.prototype.on = function (elements, eventType, handler) {
    [].forEach.call(elements, function (element) {
    element.addEventListener(eventType, handler.bind(element, this), false);
  }.bind(this));
};

DragNSort.prototype.onDragStart = function (context, event) {
  const target = getElement(event.x, event.y);

  fromX = event.x;
  fromY = event.y;

  console.log(
    `[from] id:${getAttr(target, 'data-id')}`,
    `index:${getAttr(target, 'data-index')}`,
    `is-pinned:${getAttr(target, 'data-is-pinned')}`,
    `is-merge:${target.classList.contains('handler')}`
  );

  context.$activeItem = this;

  this.classList.add(context.dragStartClass);

  event.dataTransfer.effectAllowed = 'move';
  event.dataTransfer.setData('text/html', this.innerHTML);
};

DragNSort.prototype.onDragEnd = function (context, event) {
  toX = event.x;
  toY = event.y;

  const childA = getElement(fromX, fromY);
  const childB = getElement(toX, toY);

  if (!childB.classList.contains('drag-item')) return;

  console.log(
    `[to] id:${getAttr(childB, 'data-id')}`,
    `index:${getAttr(childB, 'data-index')}`,
    `is-pinned:${getAttr(childB, 'data-is-pinned')}`
  );

  this.classList.remove(context.dragStartClass);

  const toIndex = getAttr(childB, 'data-index');
  const fromIndex = getAttr(childA, 'data-index');

  // small math part
  finalChildAStyle[Number(fromIndex)].x += childB.documentOffsetLeft - childA.documentOffsetLeft;
  finalChildAStyle[Number(fromIndex)].y += childB.documentOffsetTop - childA.documentOffsetTop;
  finalChildBStyle[Number(toIndex)].x += childA.documentOffsetLeft - childB.documentOffsetLeft;
  finalChildBStyle[Number(toIndex)].y += childA.documentOffsetTop - childB.documentOffsetTop;

  childA.style.transform = `translate(${finalChildAStyle[fromIndex].x}px, ${finalChildAStyle[fromIndex].y}px)`;
  childB.style.transform = `translate(${finalChildBStyle[toIndex].x}px, ${finalChildBStyle[toIndex].y}px)`;

  console.log(finalChildBStyle, finalChildAStyle);
};

DragNSort.prototype.onDragEnter = function (context) {
  this.classList.add(context.dragEnterClass);
};

DragNSort.prototype.onDragLeave = function (context) {
  this.classList.remove(context.dragEnterClass);
};

DragNSort.prototype.onDragOver = function (context, event) {
  if (event.preventDefault) {
    event.preventDefault();
  }

  event.dataTransfer.dropEffect = 'move';

  return false;
};

DragNSort.prototype.onDrop = function (context, event) {
  if (event.stopPropagation) {
    event.stopPropagation();
  }

  // here should be sorting or moving of array items

  context.removeClasses();

  return false;
};

DragNSort.prototype.bind = function () {
  this.on(this.$items, 'dragstart', this.onDragStart);
  this.on(this.$items, 'dragend', this.onDragEnd);
  this.on(this.$items, 'dragover', this.onDragOver);
  this.on(this.$items, 'dragenter', this.onDragEnter);
  this.on(this.$items, 'dragleave', this.onDragLeave);
  this.on(this.$items, 'drop', this.onDrop);
};

DragNSort.prototype.init = function () {
  createArray();
  this.bind();
};

// Instantiate
const pinned = new DragNSort({
  container: document.querySelector('.drag-list'),
  itemClass: 'drag-item',
  dragStartClass: 'drag-start',
  dragEnterClass: 'drag-enter',
});

const unpinned = new DragNSort({
  container: document.querySelector('.drag-list2'),
  itemClass: 'drag-item',
  dragStartClass: 'drag-start',
  dragEnterClass: 'drag-enter',
});

pinned.init();
unpinned.init();
